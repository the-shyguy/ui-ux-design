import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-timeline-card',
  templateUrl: './timeline-card.component.html',
  styleUrls: ['./timeline-card.component.css'],
})
export class TimelineCardComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
  active: boolean = false;

  line: boolean = true;
  toggle_line() {
    this.line = !this.line;
  }
}
