import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  ngOnInit(): void {}
  isDark: boolean = false;
  themeSwitch() {
    this.isDark = !this.isDark;
  }

  @Output() sidebar = new EventEmitter();
  toggleSidebar() {
    this.sidebar.emit();
  }
}
