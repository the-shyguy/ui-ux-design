import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatNativeDateModule } from '@angular/material/core';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalenderMainComponent } from './calender-main/calender-main.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BookingComponent } from './booking/booking.component';
import { UserheadComponent } from './userhead/userhead.component';
import { CalenderComponent } from './calender/calender.component';
import { TimelineComponent } from './timeline/timeline.component';

import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatBadgeModule } from '@angular/material/badge';
import { TimelineCardComponent } from './timeline-card/timeline-card.component';

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    CalenderMainComponent,
    HeaderComponent,
    DashboardComponent,
    BookingComponent,
    UserheadComponent,
    CalenderComponent,
    TimelineComponent,
    TimelineCardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatChipsModule,
    MatSlideToggleModule,
    MatIconModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatBadgeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
