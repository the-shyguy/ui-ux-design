import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'UI Design';
  isVisible: boolean = false;
  toggle() {
    console.log(this.isVisible);

    this.isVisible = !this.isVisible;
  }
}
