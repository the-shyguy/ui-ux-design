import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-calender-main',
  templateUrl: './calender-main.component.html',
  styleUrls: ['./calender-main.component.css'],
})
export class CalenderMainComponent implements OnInit {
  constructor() {}
  ngOnInit(): void {}
  @Input() collapseSidebar: boolean | undefined;
  @Output() iscollapsed = new EventEmitter();
  collapse() {
    this.collapseSidebar = !this.collapseSidebar;
    console.log(this.collapseSidebar);
    this.iscollapsed.emit();
  }
}
